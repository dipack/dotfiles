#!/usr/bin/env bash

#
# Author      : Dipack P Panjabi
# Description : This script is used to sync my Dropbox to my backup storage on Google Drive, and Backblaze B2.
#

readonly source="/home/dipack/Dropbox/";
readonly gdrive="/home/dipack/google-drive/dp/Dropbox-Mirror";
readonly b2="/home/dipack/backblaze-remote";

readonly remote_gdrive="dp-remote:Dropbox-Mirror";
readonly remote_b2="dp-b2:dipack-personal";

function main {
    remotes=("$remote_gdrive" "$remote_b2");
    for remote in "${remotes[@]}"; do
        echo "Testing RClone connection to $remote";
        if rclone -v lsf "$remote" &> /dev/null ; then
            echo "[SUCCESS] RClone connection to $remote is working as expected";
        else
            echo "[ERROR] RClone connection to $remote is not working!";
            exit 255;
        fi
    done
    
    # --archive         : Basically copies the files and subdirs in archive mode, i.e. preserving perms, etc.
    # --one-file-system : Prevent moving across different FS types, i.e. if an NTFS partition is mounted at the $source, and the current FS is Ext4, do not look at the NTFS partition
    # --info=progress2  : Provides a progress log to show you what is happening
    # The trailing slash at the end of `$source` is important, as it prevents a folder with the $source's name from being created on the $target
    rsync --archive --update --delete --one-file-system --no-prune-empty-dirs --info=progress2 --exclude=".*" --exclude="Skydiving" --exclude=".dropbox" --exclude="~*" $source/ $gdrive;

    for remote in "${remotes[@]}"; do
        # RClone has a nice little known bug; it does not sync empty directories from the $source to the $target
        # Only workaround is to create some non-empty files in the directories, to get them to sync
        if rclone sync --fast-list --drive-skip-gdocs --stats=10s --stats-log-level=NOTICE $gdrive "$remote"; then
            echo
            echo "Completed RClone sync to $remote";
            echo
        fi
    done
}

main;
