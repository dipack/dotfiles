#!/usr/bin/env bash
rsync --archive --update --delete --one-file-system --no-prune-empty-dirs --info=progress2 ~/projects/college/ ~/Dropbox/College/UB/courses --exclude="cse-676" "$@"
