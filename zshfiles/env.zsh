
# Environment Variables

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh
export ZSH_CONF_HOME=$HOME/zshfiles
export EDITOR="vim"
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
export SSH_ASKPASS="/usr/bin/ksshaskpass"
export LESS='-RMi'
export NPM_PACKAGES="$HOME/.npm-global/bin"
export USR_LOCAL_BIN_DIR="/usr/local/bin"
export USER_LOCAL_BIN_DIR="$HOME/.local/bin"
export USER_BIN_DIR="$HOME/bin"
export RUSTUP_BIN_DIR="$HOME/.rustup/toolchains/nightly-x86_64-unknown-linux-gnu/bin/"
export PATH="$USER_BIN_DIR:$RUSTUP_BIN_DIR:$NPM_PACKAGES:$USER_LOCAL_BIN_DIR:$USR_LOCAL_BIN_DIR:$PATH"
export GOPATH="$HOME/.local/go"

unset MANPATH
export MANPATH="$NPM_PACKAGES/share/man:$(manpath)"
# export TERM=xterm-256color

if [ "$ZSH_THEME" = "powerlevel9k/powerlevel9k" ]; then
    # Lifted from: https://github.com/bhilburn/powerlevel9k
    POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context dir vcs)
    POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status root_indicator virtualenv)
fi
# End of Environment Variables
